const express=require('express');
const bodyparser=require('body-parser');

const LeaderRouter=express.Router();
LeaderRouter.use(bodyparser.json());

LeaderRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
  })
.get( (req,res,next) => {
    res.end('Will send all the Leader to you!');
})
.post( (req, res, next) => {
 res.end('Will add the Leader: ' + req.body.name + ' with details: ' + req.body.description);
})
.put( (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /Leader');
})
.delete( (req, res, next) => {
    res.end('Deleting all Leader');
});


LeaderRouter.route('/:LeaderId')
.get( (req,res,next) => {
    res.end('Will send details of the Leader: ' + req.params.LeaderId +' to you!');
})
.post( (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /Leader/'+ req.params.LeaderId);
})
.put( (req, res, next) => {
  res.write('Updating the Leader: ' + req.params.LeaderId + '\n');
  res.end('Will update the Leader: ' + req.body.name + 
        ' with details: ' + req.body.description);
})
.delete( (req, res, next) => {
    res.end('Deleting Leader: ' + req.params.LeaderId);
});


module.exports=LeaderRouter;