const express=require('express');
const http=require('http');
const morgan=require('morgan');
const hostname='localhost';
const port='3003';
const bodyparser=require('body-parser');
const dishRouter=require('./Routes/dishRouter');
const leaderRouter=require('./Routes/leaderRouter');
const promoRouter=require('./Routes/promoRouter');
const app=express();
app.use(morgan('dev'));
app.use(bodyparser.json());
app.use(express.static(__dirname + '/public'));
app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promotions', promoRouter);

  
  
  
  

const server=http.createServer(app);

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });